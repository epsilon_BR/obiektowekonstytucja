package pl.edu.agh.constitutionalizer;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by bartek on 12/2/16.
 */
public class ConstRunnerTest {

    @Test
    public void runTest1() throws Exception {
        String artykul1 = "Art. 1.\n" +
                "Rzeczpospolita Polska jest dobrem wspólnym wszystkich obywateli. \n";

        assertEquals(artykul1,  ConstRunner.run(new String[] {"-artykuł", "1"}));
    }

    @Test
    public void runTest2() throws Exception {
        String artykul5 = "Art. 5.\n" +
                "Rzeczpospolita Polska strzeże niepodległości i nienaruszalności swojego terytorium, " +
                "zapewnia wolności i prawa człowieka i obywatela oraz bezpieczeństwo obywateli, " +
                "strzeże dziedzictwa narodowego oraz zapewnia ochronę środowiska, kierując się " +
                "zasadą zrównoważonego rozwoju. \n";

        assertEquals(artykul5,  ConstRunner.run(new String[] {"-artykuł", "5"}));
    }

    @Test
    public void runTest3() throws Exception {
        String artykuly1012 = "Art. 10.\n" +
                "1. Ustrój Rzeczypospolitej Polskiej opiera się na podziale i równowadze wła" +
                "dzy ustawodawczej, władzy wykonawczej i władzy sądowniczej. \n" +
                "2. Władzę ustawodawczą sprawują Sejm i Senat, władzę wykonawczą Prezydent " +
                "Rzeczypospolitej Polskiej i Rada Ministrów, a władzę sądowniczą " +
                "sądy i trybunały. \n" +
                "Art. 11.\n" +
                "1. Rzeczpospolita Polska zapewnia wolność tworzenia i działania partii politycz" +
                "nych. Partie polityczne zrzeszają na zasadach dobrowolności i równości obywa" +
                "teli polskich w celu wpływania metodami demokratycznymi na kształtowanie " +
                "polityki państwa. \n" +
                "2. Finansowanie partii politycznych jest jawne. \n" +
                "Art. 12.\n" +
                "Rzeczpospolita Polska zapewnia wolność tworzenia i działania związków zawodo" +
                "wych, organizacji społeczno-zawodowych rolników, stowarzyszeń, ruchów obywa" +
                "telskich, innych dobrowolnych zrzeszeń oraz fundacji. \n";

        assertEquals(artykuly1012,  ConstRunner.run(new String[] {"-artykuły", "10" ,"12"}));
    }
}