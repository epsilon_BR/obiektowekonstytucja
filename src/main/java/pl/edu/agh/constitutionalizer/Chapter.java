package pl.edu.agh.constitutionalizer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bartek on 11/26/16.
 */
 class Chapter {

    private String header;
    private StringBuilder title = new StringBuilder();
    private List<Article> articles  = new ArrayList<>();

    Chapter(String header) {
        this.header = header;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(header);
        builder.append("\n");
        builder.append(title);
        builder.append("\n");
        for (Article a : articles) {
            builder.append(a.toString());
        }
        return builder.toString();
    }

    void addArticle(Article newArticle) {
        articles.add(newArticle);
    }

    boolean isEmpty() {
        return articles.isEmpty();
    }

    void appendToTile(String s) {
        title.append(s);
     }
}
