package pl.edu.agh.constitutionalizer;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

/**
 * Created by bartek on 11/26/16.
 */
class OptionsCreator {

    static Options create(){

        Options options = new Options();

        //opcja artykuły posiada 2 argumenty dlatego trzeba użyć Buildera
        //dla pozostalych nie trzeba
        Option artykuły  = Option.builder("artykuły")
                .hasArgs()
                .numberOfArgs(2)
                .build();

        options.addOption(artykuły);
        options.addOption("artykuł", true, "opcja odzytu jednego artykułu o określonym numerze");
        options.addOption("rozdział", true, "opcja odzytu jednego rozdziału o określonym numerze");
        options.addOption("p", true, "opcja odzytu jednego rozdziału o określonym numerze");
        options.addOption("całość", false, "opcja odzytu całego dokumentu");
        options.addOption("preambuła", false, "opcja odzytu preambuły");

        return options;
    }
}
