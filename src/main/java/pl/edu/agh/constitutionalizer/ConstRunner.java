package pl.edu.agh.constitutionalizer;

import org.apache.commons.cli.*;

import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.SynchronousQueue;


/**
 * Created by bartek on 11/26/16.
 */
public class ConstRunner {

    public static void main(String[] args) {

        System.out.println(run(args));

    }

    static String run(String[] args) {


        String path = "src/main/resources/konstytucja.txt";
        StringBuilder result = new StringBuilder();

        Options options = OptionsCreator.create();
        CommandLineParser parser = new DefaultParser();

        try {
            CommandLine cmd = parser.parse(options, args);

            if (cmd.getOptions().length == 0){
                throw new IllegalArgumentException();
            }

            if (cmd.hasOption("p")) {
                path = cmd.getOptionValue("p");
            }

            Constitution constitution = ConstParser.read(Paths.get(path));

            if (cmd.hasOption("całość")) {
                result.append(constitution.toString());
            } else {

                if (cmd.hasOption("preambuła")) {
                    result.append(constitution.getBeginning());
                }
                if (cmd.hasOption("artykuł")) {
                    result.append(constitution.getArticle(Integer.parseInt(cmd.getOptionValue("artykuł"))));
                }
                if (cmd.hasOption("artykuły")) {
                    String[] values = cmd.getOptionValues("artykuły");
                    result
                            .append(constitution
                                    .getArticles(Integer.parseInt(values[0]), Integer.parseInt(values[1])));
                }
                if (cmd.hasOption("rozdział")) {
                    result.append(constitution.getChapter(Integer.parseInt(cmd.getOptionValue("rozdział"))));
                }
            }
        }catch (NoSuchFileException e){
            System.out.println("Podano złą ścieżkę pliku");
        }catch (NumberFormatException e) {
            System.out.println("Źle podano argumenty wywołania programu");
        }catch (IllegalArgumentException e){
            System.out.println("Źle podano argumentów, opcje do wyboru to -artykuł (nr) -artykuły (nr1) (nr2) -rozdział (nr) -p (ścieżka)");
        } catch (ParseException | IOException e) {
            System.out.println(e.getMessage());
        }catch (Exception e){
            System.out.println(e.toString()
            );
        }


        return result.toString();
    }

}
