package pl.edu.agh.constitutionalizer;

/**
 * Created by bartek on 11/26/16.
 */
 class Article {

    private String header;

    Article(String header) {
        this.header = header;
    }

    @Override
    public String toString() {
        return header + "\n" + builder.toString() + "\n";
    }

    private StringBuilder builder = new StringBuilder();

    void addLine(String s){
       if(!Character.isLetter(s.charAt(0)) && !builder.toString().equals(""))builder.append("\n");
       if(s.endsWith("-")) builder.append(s.substring(0, s.length()-1));
       else builder.append(s).append(" ");
    }
}
