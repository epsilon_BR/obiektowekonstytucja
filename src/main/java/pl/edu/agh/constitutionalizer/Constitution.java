package pl.edu.agh.constitutionalizer;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by bartek on 11/26/16.
 */
class Constitution {

    private List<Chapter> chapters = new ArrayList<>();
    private List<Article> articles = new ArrayList<>();
    private StringBuilder beginning = new StringBuilder();

//    tu zachodzi główna część zamiany tekstu na obiekty

    void addLine(String s) {

        if (s.startsWith("Rozdzia")) {
            chapters.add(new Chapter(s));
        } else if (chapters.isEmpty()) {
            addToBeginning(s);
        } else if (getLastChapter().isEmpty() && !s.startsWith("Art.")) {
            getLastChapter().appendToTile(s);
        } else if (s.startsWith("Art.")) {
          createArticle(s);
        } else {
            getLastArticle().addLine(s);
        }
    }


    private void createArticle(String s) {
        Article newArticle = new Article(s);
        articles.add(newArticle);
        getLastChapter().addArticle(newArticle);
    }

    private void addToBeginning(String s) {
        if(s.endsWith("-"))
            beginning.append(s.substring(0, s.length()-1));
        else beginning.append(s).append("\n");
    }


    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(beginning.toString());
        for (Chapter c: chapters) {
            result.append(c.toString());
        }
        return result.toString();
    }

    private Article getLastArticle(){
        return articles.get(articles.size() - 1);
    }

    private Chapter getLastChapter(){
        return chapters.get(chapters.size() - 1);
    }

    String getBeginning() {
        return beginning.toString();
    }

    String getChapter(int chapterNb) throws IllegalArgumentException {
        if (chapterNb < 1 || chapterNb > chapters.size())
            throw new IllegalArgumentException("Wybrany rozdział nie istnieje");
        return chapters.get(chapterNb - 1).toString();
    }

    String getArticle(int articleN) throws IllegalArgumentException {
        if (articleN < 1 || articleN > articles.size())
           throw new IllegalArgumentException("Wybrany artykuł nie istnieje");
        return articles.get(articleN - 1).toString();
    }

    String getArticles(int first, int last) throws IllegalArgumentException {
        if (first < 1 || last > articles.size())
            throw new IllegalArgumentException("Wybrany zakres artykułów nie istnieje");
        StringBuilder builder = new StringBuilder();
        for (int i = first - 1; i < last; i++) {
            builder.append(articles.get(i).toString());
        }
        return builder.toString();
    }


}
