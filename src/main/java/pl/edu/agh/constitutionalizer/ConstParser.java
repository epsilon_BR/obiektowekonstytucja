package pl.edu.agh.constitutionalizer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Created by bartek on 11/26/16.
 */
 class ConstParser {


    static Constitution read(Path path) throws IOException {
        Constitution rp = new Constitution();
        try (Stream<String> lines = Files.lines(path)) {
            lines
                    .filter(p -> !Objects.equals(p, "©Kancelaria Sejmu"))
                    .filter(p -> !Objects.equals(p, "2009-11-16"))
                    .filter(p -> p.length() > 1)
                    .forEach(rp::addLine);
        }
        return rp;
    }

}